package utils

import (
	"bufio"
	"errors"
	"fmt"
	"os"
	"path/filepath"
)

/**
判断文件是否存在
*/
func FileExist(path string) bool {
	//如果返回的错误为nil,说明文件或文件夹存在
	//如果返回的错误类型使用os.IsNotExist()判断为true,说明文件或文件夹不存在
	//如果返回的错误为其它类型,则不确定是否在存在
	_, err := os.Stat(path)
	if err == nil {
		return true
	}
	return !os.IsNotExist(err)
}

/**
 * 创建文件
 * 可以自动创建目录
 */
func CreateFile(filename string, flag int, chmod uint32) (*os.File, error) {
	if len(filename) < 1 {
		return nil, errors.New("CreateFile：filename is null")
	}
	abs, _ := filepath.Abs(filename)

	if FileExist(abs) {
		return os.OpenFile(filename, flag, os.FileMode(chmod))
	}
	baseDir := filepath.Dir(abs)
	if !FileExist(baseDir) {
		err := os.MkdirAll(baseDir, 755)
		if err != nil {
			fmt.Println(baseDir, "创建失败")
			return nil, err
		}
	}
	return os.Create(filename)
}

/**
写入文件
*/
func WriteLine(filename string, content []string) {
	if len(content) == 0 {
		return
	}

	var fp *os.File
	var err error

	defer fp.Close()

	fp, err = CreateFile(filename, os.O_WRONLY|os.O_APPEND, 0666)
	if err != nil {
		fmt.Println("文件操作失败", err)
		return
	}

	writer := bufio.NewWriter(fp)

	for _, u := range content {
		n, err := writer.WriteString(fmt.Sprintln(u))
		if err != nil {
			fmt.Println("write error:", err)
		} else {
			fmt.Println("write success:", n)
		}
	}
	_ = writer.Flush()
}
