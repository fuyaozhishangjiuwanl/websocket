# websocket

#### 介绍
golang 实现websocket服务端

#### 构建应用

1. go build socket.go

#### 使用说明

go run socket.go -addr :8050 -secret xxxxx -manager admin -receive http://www.xx.cn/receive

| 参数 | 类型 | 例子 | 说明 |
| :-----| ----: | :---- | :---- |
| addr | string | :8050 | websocket服务器监听端口，默认:8050|
| secret | string | xxxxx | websocket在进行广播或者分组推送时，所需要的密钥,不设置则不能进行消息推送|
| manager | string | admin | websocket在进行新增secret，或删除secret所需要的身份标识|
| receive | string | http://www.xx.cn/receive | 设置一个回调地址，用于接收websocket服务器POST过来的客户端聊天记录，```{"type":"message","group":"666","binding_id":"","content":"具体内容"}```|



1.  访问 127.0.0.1:8050/home
2.  点击open按钮进行ws连接
3.  然后进行文本发送

#### 配置说明
```golang   
const (
	// 客户端写入消息体的超时时间
	writeWait = 10 * time.Second

	// pong消息最长等待时间
	pongWait = 60 * time.Second

	// ping消息写入最长等待时间
	pingPeriod = (pongWait * 9) / 10

	// Maximum message size allowed from peer.
	// 设置每次读入message最大读取字节数
	maxMessageSize = 1024*2
)
```
### websocket链接地址
####  ws://127.0.0.1:8050/ws?group=分组标识&bindingID=绑定ID
| 参数 | 类型 | 例子 |说明 |
| :-----| ----: | :----: | :---- |
| group | string | "聊天群" | 同一个group的客户端可以进行聊天，相当于一个group就是一个微信群聊 |
| bindingID | string | "client_1" | 可选，给客户端绑定一个业务id，通常是聊天的用户的主键 |


### 推送密钥设置
#### [POST] http://127.0.0.1:8050/secret [新增密钥]
##### header 参数
| 参数 | 类型 | 例子 |说明 |
| :-----| ----: | :----: | :---- |
| Authorization | string | "admin" | admin是在启动程序时，通过-manager 指定的 |
##### bodyForm
| 参数 | 类型 | 例子 |说明 |
| :-----| ----: | :----: | :---- |
| secret | string | "poksjuxg" | 新增一个"poksjuxg"密钥，在push接口，可以Authorization中携带，进行消息推送 |

#### [DELETE] http://127.0.0.1:8050/secret [删除密钥]
##### header 参数
| 参数 | 类型 | 例子 |说明 |
| :-----| ----: | :----: | :---- |
| Authorization | string | "admin" | admin是在启动程序时，通过-manager 指定的 |
##### body
| 参数 | 类型 | 例子 |说明 |
| :-----| ----: | :----: | :---- |
| secret | string | "poksjuxg" | 删除一个"poksjuxg"的密钥，使之在push接口没有推送权限 |


### 推送消息
#### POST http://127.0.0.1:8050/push
####  分组推送：
##### header 参数，推送身份标识，用于验证是否能够进行推送
| 参数 | 类型 | 例子 |说明 |
| :-----| ----: | :----: |----: |
| Authorization | string | 213456 | 通过http://127.0.0.1:8050/secret设置，或者在启动命令时，通过-secret命令指定的 |

##### body 参数
| 类型 | 例子 |
| :-----|:---- |
| json | ```{"type":"BROADCAST","content":"广播"}``` or ```{"type":"GROUP","content":"消息内容","group":"666"}``` |

参数说明

| 参数 | 类型 | 例子 | 说明 |
| :-----| ----: | :---- | :---- |
| type | string | "broadcast" | broadcast:广播,GROUP:分组 |
| content | string | "这是一个活动消息" | 推送内容 |
| group | string | "666" | 分组标识 |


/push 与 /secret 两个接口status code 200为成功，其它为失败

#### 前端页面
1.  ws = new WebSocket("ws://127.0.0.1:8050/ws?group=666&bindingID=xxx");
2.  group相同的客户端可以相互收接消息
3.  bindingID绑定ID，为客户端绑定一个指定ID，可以是第三方服务器用户ID等，用于身份标识
4.  发送消息格式:
```json
{"type":"message","content":"内容"}
```

| 参数 | 类型 | 例子 | 说明 |
| :-----| ----: | :---- | :---- |
| type | string | "message" | 消息类型 |
| content | string | "hello" | 发送内容 |

5. 接收消息格式
```json
{"type":"message","group":"666","binding_id":"xxx","content":"具体内容"}
```

| 参数 | 类型 | 例子 | 说明 |
| :-----| ----: | :---- | :---- |
| type | string | "message" | 消息类型 |
| content | string | "hello world" | 发送内容 |
| group | string | "666" | 分组 |
| binding_id | string | "xxx" | 绑定客户端ID，相当于发送消息的客户端标识 |
