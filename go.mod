module websocket

go 1.15

require (
    github.com/gorilla/websocket v1.4.2
	github.com/segmentio/ksuid v1.0.3
)
